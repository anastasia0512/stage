
package it.stage.macchina;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
@WebServlet("/")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ArrayList<Bevanda> bevande = new ArrayList<Bevanda>();
	
	public void init() throws ServletException {
		Bevanda b = new Bevanda();
		b.setNome("Coffee");
		b.setQuantità(5);
		bevande.add(b);
		Bevanda b1 = new Bevanda();
		b1.setNome("Cappuccino");
		b1.setQuantità(5);
		bevande.add(b1);
		Bevanda b2 = new Bevanda();
		b2.setNome("Macchiato");
		b2.setQuantità(5);
		bevande.add(b2);
		Bevanda b3 = new Bevanda();
		b3.setNome("Chocolate");
		b3.setQuantità(5);
		bevande.add(b3);
		Bevanda b4 = new Bevanda();
		b4.setNome("The");
		b4.setQuantità(5);
		bevande.add(b4);		
	}
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("bevanda", bevande);
		
		getServletConfig().getServletContext().getRequestDispatcher("/view/index.jsp").forward(request, response);;
	
		doPost(request , response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameter("button1") != null) {
			if(request.getParameter("button1").equals("Invio") ) {
				for(Bevanda bev : bevande) {
					if(request.getParameter("selezione").equals(bev.getNome())) {
						bev.remove();						
					}
				}
				getServletConfig().getServletContext().getRequestDispatcher("/view/erogazione.jsp").forward(request, response);
				}
			}
			else if(request.getParameter("button2") != null) { 
				if(request.getParameter("button2").equals("Back_end")) {
					request.setAttribute("back", bevande);
				getServletConfig().getServletContext().getRequestDispatcher("/view/back_end.jsp").forward(request, response);
			}
		
			}
			if(request.getParameter("add") != null) {
				if(request.getAttribute("add").equals("+")) {
					for(Bevanda bev : bevande) {
						bev.add();
						}
				getServletConfig().getServletContext().getRequestDispatcher("/view/back_end.jsp").forward(request, response);
			}
		}
		
	}
}
	



