package it.stage.macchina;

import java.util.*;

public class CoffeSelect {
	public List<String> getTypes(String type) {
		List<String> types=new ArrayList<String>();
	
		if(type.equals("caffe")) {
			types.add("caffe");
		}
		else if(type.equals("capuccino")) {
			types.add("capuccino");
		}
		else if(type.equals("macchiato")) {
			types.add("macchiato");
		}
		else if(type.equals("cioccolato")) {
			types.add("cioccolato");
		}
		else {
			types.add("the");
		}
		return types;
	}
}
