
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="it.stage.macchina.Bevanda" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Back End</title>
</head>
<body>
<form action="Controller.java" method="post" >
	<center>
	<% 
	ArrayList<Bevanda> back  = (ArrayList<Bevanda>) request.getAttribute("back");
	
	%>
		<table summary="" style="border:1px ; display:inline;">
			<tr>
				<th>Product</th>
				<% for(int i=0; i<back.size(); i++){ %>
				<tr>
					<td><%=back.get(i).getNome() %></td>
				</tr>	
				<%}%>
		</table>
		<table summary="" style="border:1px ; display:inline;">
			<tr>
				<th>Amount</th>
				<% for(int i=0; i<back.size(); i++){ %>
				<tr>
					<td><%=back.get(i).getQuantità()%></td>
					<td><button type="submit" name ="a" value="add" onclick="<% back.get(i).getQuantità() ;%>">+</button></td>
				</tr>
				<%}%>
		</table>
	</center>
</form>
</body>
</html>