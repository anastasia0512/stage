<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Print array of char using bean</title>
</head>
<body>
	<center>
		<h2>Print array of char using bean</h2>
		<jsp:useBean id="beanarray" scope="session" class="it.stage.jsp.ArrayBean"></jsp:useBean>
		<jsp:getProperty property="array" name="beanarray"/>
	</center>
</body>
</html>