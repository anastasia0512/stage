<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import = "java.io.*,java.util.*" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Http Header Request Example</title>
</head>
<body>
	<center>
		<h2>Http Header Request Example</h2>
		
		<table width="100%" border="1" align="center">
			<tr bgcolor="pink">
				<th>Header Name</th>
				<th>Header Values</th>
			</tr>
			<%
				Enumeration headersNames=request.getHeaderNames();
				while(headersNames.hasMoreElements()){
					String paramName = (String) headersNames.nextElement();
					out.println("<tr><td>"+paramName+"</td>\n");
					String paramValue = request.getHeader(paramName);
					out.println("<td>"+paramValue+"</td></tr>\n");
				}
			%>
		</table>
	</center>
</body>
</html>