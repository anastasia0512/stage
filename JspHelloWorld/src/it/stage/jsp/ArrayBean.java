package it.stage.jsp;

public class ArrayBean implements java.io.Serializable {
	/**
	 * Print array of char with JavaBean
	 */
	private static final long serialVersionUID = 1L;
	
	public char[] array= {'c','i','a','o'};;
	
	public void setArray(char[] array) {
		this.array=array;
	}
	public String getArray() {
		String newArray = new String (array);
		return newArray;
	}
}
