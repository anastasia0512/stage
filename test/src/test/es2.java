package test;
/*Esercizio : dato un array fare una copia cosi che si modifichi 
quello originale nel caso di cambiamento della copia*/

import java.util.Arrays;
import java.util.Scanner;

public class es2 {
	public static void main(String[] args) {
		int[] array = {1,2,3,4,5};
		int[] array2 = {1,2,3};
		boolean res ;
		System.out.println("L'array:"+Arrays.toString(array2));
		
		res = CopiaUsandoClone(array2 , array);
		System.out.println(res);
	}
	
	public static boolean CopiaUsandoClone(int[] array2 , int[] array) {
		return array2.equals(array);
	}
}