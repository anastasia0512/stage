package test;
import java.io.*;
import java.util.*;

public class SequenzaFibonacci {
	public static int Fibonacci(int n) {
		int precedente = 1 ;
		int successivo = 1 ;
		int somma = 1 ;
		
		
		if (n>1)
			for (int i=2;i<n;i++) {
				somma = precedente + successivo;
				precedente = successivo;
				successivo = somma;
			}
		return somma;
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Inserisci il valore n :");
		int n = input.nextInt();
		for (int i=0 ; i<n ; i++) {
		System.out.println("La sequenza di fibonacci e':" + Fibonacci(i));
		}
	}
}
